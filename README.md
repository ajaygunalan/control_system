# Control System Lecture Notes

### Video Lectures

1. Steve Brunton: [Videos](https://www.youtube.com/playlist?list=PLMrJAkhIeNNR20Mz-VpzgfQs5zrYi085m)

2. Brian Douglas: [Videos](https://www.youtube.com/playlist?list=PLUMWjy5jgHK1NC52DXXrriwihVrYZKqjk)

### Books

1. Norman Nise
2. Ogata
